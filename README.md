Простой клиент для взаимодействия с некоторыми внешними ресурсами.
------
Реализована поддержка следующих эндпоинтов:
- example.com:
    - GET /comments
    - POST /comment
    - PUT /comment
- api.hh.ru:
    - GET /professional_roles

Для работы необходим PHP 7.4.

Установка: 
 ```bash 
 composer require skripnikov/example-com-client:dev-master
 ```

Пример использования:
 ```php
use Skripnikov\ExampleComClient\Service\ExampleComRequestService;
use Skripnikov\ExampleComClient\Service\HhRuRequestService;

$exampleComService = new ExampleComRequestService('bearerTok#nEx4mpl#');
$hhRuService       = new HhRuRequestService();

$professionalRoles = $hhRuService->getProfessionalRoles();

$comments          = $exampleComService->getComments();
$newComment        = $exampleComService->createComment('SomeName', 'Text');
$updatedComment    = $exampleComService->changeComment('UpdatedName', 'Updated text', 33);
 ```
Для использования API example.com требуется Bearer токен.