<?php

namespace Skripnikov\ExampleComClient\Tests\Client;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\Exception\RequestException;
use GuzzleHttp\Handler\MockHandler;
use GuzzleHttp\HandlerStack;
use GuzzleHttp\Psr7\Request;
use GuzzleHttp\Psr7\Response;
use PHPUnit\Framework\TestCase;
use Skripnikov\ExampleComClient\Client\GuzzleHttpClient;
use Skripnikov\ExampleComClient\Exception\HttpRequestException;

class GuzzleHttpClientTest extends TestCase
{
    /**
     * @throws HttpRequestException
     */
    public function testExecuteHttpRequestSuccess(): void
    {
        $method = 'GET';
        $baseUrl = 'https://example.com';
        $endpoint = '/api/v1/some-endpoint';
        $jsonData = '{ "data": "some data" }';

        $mock = new MockHandler([
            new Response(200, [], '{ "data": "response data" }'),
        ]);
        $handler = HandlerStack::create($mock);
        $guzzleClient = new GuzzleClient(['handler' => $handler]);

        $httpClient = new GuzzleHttpClient();
        $httpClient->setGuzzleClient($guzzleClient);

        $response = $httpClient->executeHttpRequest($method, $baseUrl, $endpoint, $jsonData);

        $this->assertEquals(200, $response->getStatusCode());
        $this->assertEquals('{ "data": "response data" }', $response->getBody()->getContents());
    }

    public function testExecuteHttpRequestThrowsException(): void
    {
        $this->expectException(HttpRequestException::class);
        $this->expectExceptionMessage('An error occurred while sending http request: test exception');

        $method = 'GET';
        $baseUrl = 'http://example.com/api';
        $endpoint = '/test';
        $jsonData = '{}';

        $mock = new MockHandler([
            new RequestException("test exception", new Request($method, $baseUrl . $endpoint)),
        ]);

        $handlerStack = HandlerStack::create($mock);
        $client = new GuzzleClient(['handler' => $handlerStack]);

        $guzzleHttpClient = new GuzzleHttpClient();
        $guzzleHttpClient->setGuzzleClient($client);

        $guzzleHttpClient->executeHttpRequest($method, $baseUrl, $endpoint, $jsonData);
    }
}