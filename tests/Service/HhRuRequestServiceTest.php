<?php

use PHPUnit\Framework\TestCase;
use Skripnikov\ExampleComClient\Model\HhRu\ProfessionalRolesResponse;
use Skripnikov\ExampleComClient\Service\HhRuRequestService;

final class HhRuRequestServiceTest extends TestCase
{
    private HhRuRequestService $hhRuRequestService;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->hhRuRequestService = new HhRuRequestService();
    }

    public function testCanGetProfessionalRoles(): void
    {
        $professionalRoles = $this->hhRuRequestService->getProfessionalRoles();

        $this->assertInstanceOf(ProfessionalRolesResponse::class, $professionalRoles);
    }
}