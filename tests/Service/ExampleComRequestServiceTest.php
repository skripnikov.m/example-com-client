<?php

use PHPUnit\Framework\TestCase;
use Skripnikov\ExampleComClient\Model\ExampleCom\Comment;
use Skripnikov\ExampleComClient\Model\ExampleCom\CommentsResponseModel;
use Skripnikov\ExampleComClient\Service\ExampleComRequestService;

final class ExampleComRequestServiceTest extends TestCase
{
    private ExampleComRequestService $exampleComRequestService;

    /**
     * @before
     */
    public function setup(): void
    {
        $this->exampleComRequestService = new ExampleComRequestService('bearerTok#nEx4mpl#');
    }

    public function testCannotCreatedWithoutBearer(): void
    {
        $this->expectException(ArgumentCountError::class);

        new ExampleComRequestService();
    }

    public function testGetComments(): void
    {
        $response = $this->exampleComRequestService->getComments();

        $this->assertInstanceOf(CommentsResponseModel::class, $response);
    }

    public function testCreateComment(): void
    {
        $name = 'test';
        $text = 'test';

        $response = $this->exampleComRequestService->createComment('test', 'test');

        $this->assertInstanceOf(Comment::class, $response);
        $this->assertEquals($name, $response->getName());
        $this->assertEquals($text, $response->getText());
    }

    public function testChangeComment()
    {
        $name = 'test';
        $text = 'test';
        $id = 1;

        $response = $this->exampleComRequestService->changeComment($name, $text, $id);

        $this->assertInstanceOf(Comment::class, $response);
        $this->assertEquals($name, $response->getName());
        $this->assertEquals($text, $response->getText());
        $this->assertEquals($id, $response->getId());
    }
}