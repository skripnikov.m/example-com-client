<?php

namespace Skripnikov\ExampleComClient\Client;

use Psr\Http\Message\ResponseInterface;
use Skripnikov\ExampleComClient\Exception\HttpRequestException;

interface HttpClientInterface
{
    /**
     * @param string $method
     * @param string $baseUrl
     * @param string $endpoint
     * @param string $jsonData
     *
     * @return ResponseInterface
     * @throws HttpRequestException
     */
    public function executeHttpRequest(string $method, string $baseUrl, string $endpoint, string $jsonData): ResponseInterface;
}
