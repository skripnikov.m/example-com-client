<?php

namespace Skripnikov\ExampleComClient\Client;

use GuzzleHttp\Client as GuzzleClient;
use GuzzleHttp\RequestOptions;
use Psr\Http\Message\ResponseInterface;
use Skripnikov\ExampleComClient\Exception\HttpRequestException;
use Throwable;

/**
 * Class GuzzleHttpClient.
 */
class GuzzleHttpClient implements HttpClientInterface
{
    private const GUZZLE_REQUEST_CONTENT_TYPE = 'application/json';
    private const GUZZLE_CONNECT_TIMEOUT      = 30.0;
    private const GUZZLE_TIMEOUT              = 240.0;

    /**
     * @var string|null
     */
    protected ?string $bearerToken;

    /**
     * @var GuzzleClient|null
     */
    private ?GuzzleClient $guzzleClient = null;

    /**
     * GuzzleHttpClient constructor.
     *
     * @param ?string $bearerToken
     */
    public function __construct(?string $bearerToken = null)
    {
        $this->bearerToken = $bearerToken;
    }

    /**
     * @param string $method
     * @param string $baseUrl
     * @param string $endpoint
     * @param string $jsonData
     *
     * @return ResponseInterface
     * @throws HttpRequestException
     */
    public function executeHttpRequest(string $method, string $baseUrl, string $endpoint, string $jsonData): ResponseInterface
    {
        try {
            $response = $this->getGuzzleClient()->request($method, $baseUrl . $endpoint, [
                RequestOptions::BODY => $jsonData,
            ]);
        } catch (Throwable $exception) {
            throw new HttpRequestException("An error occurred while sending http request: " . $exception->getMessage());
        }

        return $response;
    }

    /**
     * @return GuzzleClient
     */
    private function getGuzzleClient(): GuzzleClient
    {
        if ($this->guzzleClient === null) {
            $this->guzzleClient = $this->createGuzzleClient();
        }

        return $this->guzzleClient;
    }

    /**
     * @param GuzzleClient|null $guzzleClient
     *
     * @return $this
     */
    public function setGuzzleClient(?GuzzleClient $guzzleClient): self
    {
        $this->guzzleClient = $guzzleClient;

        return $this;
    }

    private function createGuzzleClient(): GuzzleClient
    {
        $clientOptions = [
            RequestOptions::CONNECT_TIMEOUT => self::GUZZLE_CONNECT_TIMEOUT,
            RequestOptions::TIMEOUT         => self::GUZZLE_TIMEOUT,
            RequestOptions::HEADERS         => [
                'Content-Type'  => self::GUZZLE_REQUEST_CONTENT_TYPE,
                'Accept'        => self::GUZZLE_REQUEST_CONTENT_TYPE,
            ],
        ];

        if ($this->bearerToken !== null) {
            $clientOptions[RequestOptions::HEADERS]['Authorization'] = 'Bearer ' . $this->bearerToken;
        }

        return new GuzzleClient($clientOptions);
    }
}
