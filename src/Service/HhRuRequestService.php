<?php

namespace Skripnikov\ExampleComClient\Service;

use JMS\Serializer\SerializerBuilder;
use Skripnikov\ExampleComClient\Client\GuzzleHttpClient;
use Skripnikov\ExampleComClient\Exception\HttpRequestException;
use Skripnikov\ExampleComClient\Model\HhRu\ProfessionalRolesResponse;
use Skripnikov\ExampleComClient\Request\HhRu\GetProfessionalRolesRequest;
use stdClass;

class HhRuRequestService
{
    private GetProfessionalRolesRequest $getProfessionalRolesRequest;

    public function __construct()
    {
        $client = new GuzzleHttpClient();
        $serializer = SerializerBuilder::create()->build();

        $this->getProfessionalRolesRequest = new GetProfessionalRolesRequest(
            $serializer,
            $client
        );
    }

    /**
     * @return ProfessionalRolesResponse
     *
     * @throws HttpRequestException
     */
    public function getProfessionalRoles(): ProfessionalRolesResponse
    {
        $this->getProfessionalRolesRequest->executeRequestToResource(new stdClass());

        return $this->getProfessionalRolesRequest->getDeserializedResponseBody();
    }
}
