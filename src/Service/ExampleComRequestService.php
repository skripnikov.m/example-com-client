<?php

namespace Skripnikov\ExampleComClient\Service;

use JMS\Serializer\SerializerBuilder;
use Skripnikov\ExampleComClient\Client\GuzzleHttpClient;
use Skripnikov\ExampleComClient\Exception\HttpRequestException;
use Skripnikov\ExampleComClient\Model\ExampleCom\Comment;
use Skripnikov\ExampleComClient\Model\ExampleCom\CommentsResponseModel;
use Skripnikov\ExampleComClient\Request\ExampleCom\GetCommentsExampleComRequest;
use Skripnikov\ExampleComClient\Request\ExampleCom\PostCommentExampleComRequest;
use Skripnikov\ExampleComClient\Request\ExampleCom\PutCommentExampleComRequest;
use stdClass;

class ExampleComRequestService
{
    private GetCommentsExampleComRequest $getCommentsExampleComRequest;

    private PostCommentExampleComRequest $postCommentExampleComRequest;

    private PutCommentExampleComRequest $putCommentExampleComRequest;

    public function __construct($bearerToken)
    {
        $client = new GuzzleHttpClient($bearerToken);
        $serializer = SerializerBuilder::create()->build();

        $this->getCommentsExampleComRequest = new GetCommentsExampleComRequest(
            $serializer,
            $client
        );

        $this->postCommentExampleComRequest = new PostCommentExampleComRequest(
            $serializer,
            $client
        );

        $this->putCommentExampleComRequest = new PutCommentExampleComRequest(
            $serializer,
            $client
        );
    }

    /**
     * @return CommentsResponseModel
     *
     * @throws HttpRequestException
     */
    public function getComments(): CommentsResponseModel
    {
        $this->getCommentsExampleComRequest->executeRequestToResource(new stdClass());

        return $this->getCommentsExampleComRequest->getDeserializedResponseBody();
    }

    /**
     * @param $name
     * @param $text
     *
     * @return Comment
     *
     * @throws HttpRequestException
     */
    public function createComment($name, $text): Comment
    {
        $comment = Comment::create($name, $text);

        $this->postCommentExampleComRequest->executeRequestToResource($comment);

        return $this->postCommentExampleComRequest->getDeserializedResponseBody();
    }

    /**
     * @param $name
     * @param $text
     * @param $id
     *
     * @return Comment
     *
     * @throws HttpRequestException
     */
    public function changeComment($name, $text, $id): Comment
    {
        $comment = Comment::create($name, $text, $id);

        $this->putCommentExampleComRequest->executeRequestToResource($comment);

        return $this->putCommentExampleComRequest->getDeserializedResponseBody();
    }
}
