<?php

namespace Skripnikov\ExampleComClient\Request\HhRu;

/**
 * Trait HhRuTrait.
 */
trait HhRuTrait
{
    /**
     * @return string
     */
    final protected function getRequestBaseUrl(): string
    {
        return 'https://api.hh.ru/';
    }
}
