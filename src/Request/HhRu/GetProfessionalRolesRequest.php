<?php

namespace Skripnikov\ExampleComClient\Request\HhRu;

use Skripnikov\ExampleComClient\Model\HhRu\ProfessionalRolesResponse;
use Skripnikov\ExampleComClient\Request\AbstractHttpRequest;

class GetProfessionalRolesRequest extends AbstractHttpRequest
{
    use HhRuTrait;

    /**
     * @return string
     */
    final protected function getRequestEndpoint(): string
    {
        return 'professional_roles';
    }

    /**
     * @return string
     */
    final protected function getRequestMethod(): string
    {
        return 'GET';
    }

    final protected function getResponseModelName(): string
    {
        return ProfessionalRolesResponse::class;
    }
}
