<?php

namespace Skripnikov\ExampleComClient\Request;

use Skripnikov\ExampleComClient\Client\HttpClientInterface;
use JMS\Serializer\SerializerInterface;
use Psr\Http\Message\ResponseInterface;
use Skripnikov\ExampleComClient\Exception\HttpRequestException;

/**
 * Class AbstractHttpRequest.
 */
abstract class AbstractHttpRequest
{
    public const DEFAULT_SERIALIZER_FORMAT = 'json';

    /**
     * @var HttpClientInterface
     */
    protected HttpClientInterface $client;

    /**
     * @var SerializerInterface
     */
    protected SerializerInterface $serializer;

    /**
     * @var ResponseInterface|null
     */
    protected ?ResponseInterface $response = null;

    /**
     * AbstractHttpRequest constructor.
     *
     * @param SerializerInterface $serializer
     * @param HttpClientInterface $client
     */
    public function __construct(
        SerializerInterface $serializer,
        HttpClientInterface $client
    ) {
        $this->serializer = $serializer;
        $this->client     = $client;
    }

    /**
     * @param object $requestModel
     * @param bool $andSave
     *
     * @return ResponseInterface
     *
     * @throws HttpRequestException
     */
    public function executeRequestToResource(object $requestModel, bool $andSave = true): ResponseInterface
    {
        $response = $this->client->executeHttpRequest(
            $this->getRequestMethod(),
            $this->getRequestBaseUrl(),
            $this->getRequestEndpoint(),
            $this->getSerializedRequestBody($requestModel)
        );

        if ($andSave === true) {
            $this->response = $response;
        }

        return $response;
    }

    /**
     * @return object
     */
    public function getDeserializedResponseBody(): object
    {
        return $this->serializer->deserialize(
            $this->getResponse()->getBody(),
            $this->getResponseModelName(),
            self::DEFAULT_SERIALIZER_FORMAT
        );
    }

    /**
     * @param object $requestModel
     *
     * @return string
     */
    public function getSerializedRequestBody(object $requestModel): string
    {
        return $this->serializer->serialize($requestModel, self::DEFAULT_SERIALIZER_FORMAT);
    }

    /**
     * @return ResponseInterface|null
     */
    public function getResponse(): ?ResponseInterface
    {
        return $this->response;
    }

    /**
     * @return string
     */
    abstract protected function getRequestEndpoint(): string;

    /**
     * @return string
     */
    abstract protected function getRequestBaseUrl(): string;

    /**
     * @return string
     */
    abstract protected function getRequestMethod(): string;

    /**
     * @return string
     */
    abstract protected function getResponseModelName(): string;
}
