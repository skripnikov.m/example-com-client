<?php

namespace Skripnikov\ExampleComClient\Request\ExampleCom;

/**
 * Trait ResourceTrait.
 */
trait ExampleComTrait
{
    /**
     * @return string
     */
    final protected function getRequestBaseUrl(): string
    {
        return 'https://example.com/';
    }
}
