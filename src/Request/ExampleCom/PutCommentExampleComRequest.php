<?php

namespace Skripnikov\ExampleComClient\Request\ExampleCom;

use Skripnikov\ExampleComClient\Exception\NullCommentIdException;
use Skripnikov\ExampleComClient\Model\ExampleCom\Comment;
use Skripnikov\ExampleComClient\Request\AbstractHttpRequest;

class PutCommentExampleComRequest extends AbstractHttpRequest
{
    use ExampleComTrait;

    /**
     * @var Comment
     */
    private Comment $comment;

    /**
     * @param Comment $comment
     * @return void
     */
    public function setComment(Comment $comment): void
    {
        $this->comment = $comment;
    }

    /**
     * @return string
     * @throws NullCommentIdException
     */
    final protected function getRequestEndpoint(): string
    {
        $commentId = $this->comment->getId();

        if ($commentId === null) {
            throw new NullCommentIdException('Comment ID must not be empty!');
        }

        return 'comment/' . $commentId;
    }

    /**
     * @return string
     */
    final protected function getRequestMethod(): string
    {
        return 'PUT';
    }

    final protected function getResponseModelName(): string
    {
        return Comment::class;
    }
}
