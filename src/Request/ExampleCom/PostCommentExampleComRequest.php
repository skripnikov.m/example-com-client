<?php

namespace Skripnikov\ExampleComClient\Request\ExampleCom;

use Skripnikov\ExampleComClient\Model\ExampleCom\Comment;
use Skripnikov\ExampleComClient\Request\AbstractHttpRequest;

class PostCommentExampleComRequest extends AbstractHttpRequest
{
    use ExampleComTrait;

    /**
     * @return string
     */
    final protected function getRequestEndpoint(): string
    {
        return 'comment';
    }

    /**
     * @return string
     */
    final protected function getRequestMethod(): string
    {
        return 'POST';
    }

    final protected function getResponseModelName(): string
    {
        return Comment::class;
    }
}
