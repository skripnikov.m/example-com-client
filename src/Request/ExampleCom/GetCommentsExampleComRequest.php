<?php

namespace Skripnikov\ExampleComClient\Request\ExampleCom;

use Skripnikov\ExampleComClient\Model\ExampleCom\CommentsResponseModel;
use Skripnikov\ExampleComClient\Request\AbstractHttpRequest;

class GetCommentsExampleComRequest extends AbstractHttpRequest
{
    use ExampleComTrait;

    /**
     * @return string
     */
    final protected function getRequestEndpoint(): string
    {
        return 'comments';
    }

    /**
     * @return string
     */
    final protected function getRequestMethod(): string
    {
        return 'GET';
    }

    final protected function getResponseModelName(): string
    {
        return CommentsResponseModel::class;
    }
}
