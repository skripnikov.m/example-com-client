<?php

namespace Skripnikov\ExampleComClient\Model\HhRu;

use JMS\Serializer\Annotation as Serializer;

class ProfessionalRolesResponse
{
    /**
     * @var array|Category[]
     *
     * @Serializer\Expose()
     * @Serializer\Type("array<Skripnikov\ExampleComClient\Model\HhRu\Category>")
     */
    private array $categories;

    /**
     * @param array $categories
     *
     * @return $this
     */
    public function setCategories(array $categories): self
    {
        $this->categories = $categories;

        return $this;
    }

    /**
     * @return array
     */
    public function getCategories(): array
    {
        return $this->categories;
    }
}
