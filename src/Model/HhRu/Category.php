<?php

namespace Skripnikov\ExampleComClient\Model\HhRu;

use JMS\Serializer\Annotation as Serializer;

class Category
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     */
    private int $id;

    /**
     * @var string
     *
     * @Serializer\Expose()
     */
    private string $name;

    /**
     * @var array|Role[]
     *
     * @Serializer\Expose()
     * @Serializer\Type("array<Skripnikov\ExampleComClient\Model\HhRu\Role>")
     */
    private array $roles;

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param array $roles
     *
     * @return $this
     */
    public function setRoles(array $roles): self
    {
        $this->roles = $roles;

        return $this;
    }

    /**
     * @return array
     */
    public function getRoles(): array
    {
        return $this->roles;
    }
}
