<?php

namespace Skripnikov\ExampleComClient\Model\HhRu;

use JMS\Serializer\Annotation as Serializer;

class Role
{
    /**
     * @var int
     *
     * @Serializer\Expose()
     */
    private int $id;

    /**
     * @var string
     *
     * @Serializer\Expose()
     */
    private string $name;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     */
    private bool $isDefault;

    /**
     * @var bool
     *
     * @Serializer\Expose()
     */
    private bool $acceptIncompleteResumes;

    /**
     * @param int $id
     *
     * @return $this
     */
    public function setId(int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param bool $isDefault
     *
     * @return $this
     */
    public function setIsDefault(bool $isDefault): self
    {
        $this->isDefault = $isDefault;

        return $this;
    }

    /**
     * @return bool
     */
    public function isIsDefault(): bool
    {
        return $this->isDefault;
    }

    /**
     * @param bool $acceptIncompleteResumes
     *
     * @return $this
     */
    public function setAcceptIncompleteResumes(bool $acceptIncompleteResumes): self
    {
        $this->acceptIncompleteResumes = $acceptIncompleteResumes;

        return $this;
    }

    /**
     * @return bool
     */
    public function isAcceptIncompleteResumes(): bool
    {
        return $this->acceptIncompleteResumes;
    }
}
