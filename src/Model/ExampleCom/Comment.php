<?php

namespace Skripnikov\ExampleComClient\Model\ExampleCom;

use JMS\Serializer\Annotation as Serializer;

class Comment
{
    /**
     * @var int|null
     * @Serializer\Expose()
     */
    private ?int $id;

    /**
     * @var string
     * @Serializer\Expose()
     */
    private string $name;

    /**
     * @var string
     * @Serializer\Expose()
     */
    private string $text;

    public static function create(string $name, string $text, ?int $id = null): self
    {
        $comment = new self();

        $comment->setId($id);
        $comment->setName($name);
        $comment->setText($text);

        return $comment;
    }

    /**
     * @param int|null $id
     *
     * @return $this
     */
    public function setId(?int $id): self
    {
        $this->id = $id;

        return $this;
    }

    /**
     * @return int|null
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * @param string $name
     *
     * @return $this
     */
    public function setName(string $name): self
    {
        $this->name = $name;

        return $this;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $text
     *
     * @return $this
     */
    public function setText(string $text): self
    {
        $this->text = $text;

        return $this;
    }

    /**
     * @return string
     */
    public function getText(): string
    {
        return $this->text;
    }
}
