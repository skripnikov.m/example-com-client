<?php

namespace Skripnikov\ExampleComClient\Model\ExampleCom;

use JMS\Serializer\Annotation as Serializer;

class CommentsResponseModel
{
    /**
     * @var array|Comment[]
     * @Serializer\Expose()
     * @Serializer\Type("array<Skripnikov\ExampleComClient\Model\ExampleCom\Comment>")
     */
    private array $comments;

    /**
     * @param array $comments
     *
     * @return $this
     */
    public function setComments(array $comments): self
    {
        $this->comments = $comments;

        return $this;
    }

    /**
     * @return array
     */
    public function getComments(): array
    {
        return $this->comments;
    }
}
