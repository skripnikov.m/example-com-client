<?php

namespace Skripnikov\ExampleComClient\Exception;

use Exception;

/**
 * Class HttpRequestException.
 */
class HttpRequestException extends Exception
{
}
