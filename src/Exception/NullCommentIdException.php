<?php

namespace Skripnikov\ExampleComClient\Exception;

use Exception;

/**
 * Class NullCommentIdException.
 */
class NullCommentIdException extends Exception
{
}
